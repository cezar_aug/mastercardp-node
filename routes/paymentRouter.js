var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var Simplify = require("simplify-commerce");
var config = require('../config.js');
var Verify = require('./verify');
var User = require('../models/user');

var Payment = require('../models/payment');

var paymentRouter = express.Router();
paymentRouter.use(bodyParser.json());


paymentRouter.route('/')
.post(Verify.verifyOrdinaryUser, function(req, res, next) {
    client = Simplify.getClient({
        publicKey: config.simplify.publicKey,
        privateKey: config.simplify.privateKey
    });
    console.log(req.body);
    client.payment.create({
        amount : req.body.quantia,
        token : req.body.tokenPagamento,
        description : req.body.descricao,
        currency : "USD"
    }, function(errData, data){
    if(errData){
        console.error("Error Message: " + errData.data.error.message);
        return next(errData);
    }
    console.log("Payment Status: " + data.paymentStatus);
        
    var facebookId = "";
    User.findOne({'_id':req.decoded._id})
        .exec(function (err, user) {
        if (err) return next(err);
        var payment = {
            quantia: req.body.quantia,
            descricao: req.body.descricao,
            token: req.body.tokenPagamento,
            paganteFacebookId: user.OauthId,
            recebedorFacebookId: req.body.recebedorFacebookId
        }
        Payment.create(req.body, function (err, offer) {
            if (err) return next(err);
            console.log(offer);
        });
    });
    res.json(data.paymentStatus)
});
});


module.exports = paymentRouter;