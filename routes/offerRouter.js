var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var Verify = require('./verify');

var Offer = require('../models/offer');
var User = require('../models/user');
var Facebook = require('./facebook.js');

var offerRouter = express.Router();
offerRouter.use(bodyParser.json());


offerRouter.route('/')
.get(Verify.verifyOrdinaryUser, function(req, res, next) {
    User.findOne({'_id':req.decoded._id})
        .exec(function (err, user) {
        if (err) return next(err);
        
    Facebook.getFbData(user.OauthToken, '/me?fields=friends{picture,name}', function(retorno){
            var retorno = JSON.parse(retorno);
            var amigos = [];
            var idsFacebookAmigos = [];
            for(var i = 0; i < retorno.friends.data.length; i++){
                amigos[i] = {
                    facebookId: retorno.friends.data[i].id,
                    nome: retorno.friends.data[i].name,
                    foto: retorno.friends.data[i].picture.data.url
                };
                idsFacebookAmigos[i] = retorno.friends.data[i].id;
            }
            Offer.find({"facebookId" : {$in:idsFacebookAmigos}})
                .sort({'updatedAt': 'desc'})
                .lean()
                .exec(function (err, offers) {
                if (err) return next(err);
                
                for(var i  = 0; i < offers.length; i++){
                    console.log("Aqui");
                    console.log(amigos);
                    for(var j = 0; j < amigos.length; j++){
                        if(offers[i].facebookId == amigos[j].facebookId){
                            offers[i].amigo = amigos[j];
                            break;
                        }
                    }
                }
                
                res.json(offers);
            });
        });
    });
    
})

.post(Verify.verifyOrdinaryUser, function(req, res, next) {
    //req.body.autor = req.decoded._id;
    var facebookId = "";
    User.findOne({'_id':req.decoded._id})
        .exec(function (err, user) {
        if (err) return next(err);
        req.body.facebookId = user.OauthId;
        console.log(req.body);
        Offer.create(req.body, function (err, offer) {
            if (err) return next(err);
            console.log('offer created!');
            var id = offer._id;
            res.json(id);
        });
    });
    
});

offerRouter.route('/myOffers')
.get(Verify.verifyOrdinaryUser, function(req, res, next) {
    User.findOne({'_id':req.decoded._id})
        .exec(function (err, user) {
        if (err) return next(err);
            Offer.find({'facebookId' : user.OauthId})
                .sort({'updatedAt': 'desc'})
                .exec(function (err, offers) {
                if (err) return next(err);
                
                res.json(offers);
            });
        });
});


module.exports = offerRouter;
