var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var config = require('./config');
var authenticate = require('./authenticate');
var passport = require('passport');
var timeout = require('connect-timeout');

mongoose.connect(config.mongoUrl);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    // we're connected!
    console.log("Connected correctly to server");
});

var routes = require('./routes/index');
var users = require('./routes/users');
var offerRouter = require('./routes/offerRouter');
var paymentRouter = require('./routes/paymentRouter');

var app = express();
app.use(timeout('100s'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(haltOnTimedout);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.use(haltOnTimedout);
app.set('view engine', 'jade');
app.use(haltOnTimedout);

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(haltOnTimedout);
app.use(bodyParser.json());
app.use(haltOnTimedout);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(haltOnTimedout);
app.use(cookieParser());
app.use(haltOnTimedout);
app.use(haltOnTimedout);

// passport config
app.use(passport.initialize());


app.use('/', routes);
app.use('/users', users);
app.use('/offer', offerRouter);
app.use('/payment', paymentRouter);

function haltOnTimedout(req, res, next){
  if (!req.timedout) next();
}

app.listen(3000);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.json('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
