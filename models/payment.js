var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Offer = new Schema({
    quantia: {type:String, unique:false},
    descricao: {type:String, unique:false},
    token: {type:String, unique:false},
    paganteFacebookId: {type:String, unique:false},
    recebedorFacebookId: {type:String, unique:false}
    },{
    timestamps: true
    }
);


//Offer.plugin(passportLocalMongoose);

module.exports = mongoose.model('Payment', Offer);