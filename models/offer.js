var mongoose = require('mongoose');
var Schema = mongoose.Schema;
//var passportLocalMongoose = require('passport-local-mongoose');

var Offer = new Schema({
    titulo: {type:String, unique:false},
    descricao: {type:String, unique:false},
    foto: {type:String, unique:false},
    facebookId: {type:String, unique:false, index:false},
    ativo: {type:Boolean, default:true}
    },{
    timestamps: true
    }
);

Offer.methods.getName = function() {
    return (this.firstname + ' ' + this.lastname);
};

//Offer.plugin(passportLocalMongoose);

module.exports = mongoose.model('Offer', Offer);