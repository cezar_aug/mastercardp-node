module.exports = {
    'secretKey': '12345-67890-09876-54321',
    'mongoUrl' : 'mongodb://:@ds037622.mlab.com:37622/heroku_q8gwz44v',
    'facebook': {
        clientID: '',
        clientSecret: '',
        callbackURL: 'https://mastercarddemo.herokuapp.com/users/facebook/callback'
    },
    'simplify' : {
        publicKey: '',
        privateKey: ''
    }
}